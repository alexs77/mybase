name              'baseline'
maintainer        'Alexander Skwar'
maintainer_email  'a@skwar.me'
license           'Apache 2.0'
description       'Basis-Konfiguration meiner Systeme'
long_description  IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version           '2.0.0'
recipe            'default', 'Basic configuration for all nodes'
chef_version      "~> 12.14" if respond_to?(:chef_version)
supports          'ubuntu'

#depends           'apt'
#depends           'lvm'
depends           'openssh'
depends           'resolver'
depends           'sysctl'
depends           'logrotate'

depends           'icinga-client'

# The `issues_url` points to the location where issues for this cookbook are
# tracked.  A `View Issues` link will be displayed on this cookbook's page when
# uploaded to a Supermarket.
#
issues_url 'https://bitbucket.org/alexs77/mybase/issues?status=new&status=open'

# The `source_url` points to the development repository for this cookbook.  A
# `View Source` link will be displayed on this cookbook's page when uploaded to
# a Supermarket.
#
source_url 'https://bitbucket.org/alexs77/mybase'

