# Der zu verwendende Syslog-Server (nur für FreeBSD (?))
default["mybase"][:hardening][:syslogserver] = 'loghost'

# Anzupassende Rechte von Systemdateien
default["mybase"][:hardening][:file_permissions] = {
    :"/etc/passwd" => {
        :owner => 0,
        :group => 0,
        :perms => 0o644
    },
    :"/etc/shadow" => {
        :owner => 0,
        :group => "shadow",
        :perms => 0o640
    },
    :"/etc/group" => {
        :owner => 0,
        :group => 0,
        :perms => 0o644
    },
    :"/etc/gshadow" => {
        :owner => 0,
        :group => "shadow",
        :perms => 0o640
    }
}

# Dateien, bei denen das setuid Bit *entfernt* wird (-> Compass SNB NG Audit)
default["mybase"][:hardening][:no_setuid_files] = %w[/usr/lib/pt_chown /usr/lib/eject/dmcrypt-get-device /usr/bin/chfn]

# UMASK restriktiv setzen, in bashrc…
default["mybase"][:hardening][:umask] = "0077"
# … Pfad zur bashrc
default["mybase"][:hardening][:bashrc] = case node['os']
    when 'linux' then '/etc/bash.bashrc'
    when 'freebsd' then '/etc/bashrc'
end # of default["mybase"][:hardening][:bashrc] = case node['os']

# Shell Timeout (in Sekunden) => 10 Stunden, 2 Minuten, 1 Sekunde (mehr als 1 Arbeitstag)
default["mybase"][:hardening][:shell_timeout] = 10*60*60 + 2*60 + 1

# EOF #
