
# DNS.Watch
#default["resolver"]["nameservers"] = %w[84.200.69.80 84.200.70.40]
# FreeDNS
#default["resolver"]["nameservers"] = %w[37.235.1.174 37.235.1.177]
# Censurfridns.DK
#default["resolver"]["nameservers"] = %w[91.239.100.100 89.233.43.71]
# Quad9
#default["resolver"]["nameservers"] = %w[9.9.9.9]
# Cloudflare 1.1.1.1
default["resolver"]["nameservers"] = %w[1.1.1.1 1.0.0.1]

default["resolver"]["search"] = "localdomain"
default["resolver"]["options"] = {
    "timeout" => 2,
    "rotate" => true
}
