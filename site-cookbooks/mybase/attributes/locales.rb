# Standard Locale

default["mybase"][:locales][:default] = "LANG=de_CH.UTF-8 LANGUAGE=de_CH.UTF-8 LC_TIME=en_US.UTF-8"
default["mybase"][:locales][:supported] = %w[
    de_AT\ ISO-8859-1
    de_AT.UTF-8\ UTF-8
    de_AT@euro\ ISO-8859-15
    de_CH\ ISO-8859-1
    de_CH.UTF-8\ UTF-8
    de_DE\ ISO-8859-1
    de_DE.UTF-8\ UTF-8
    de_DE@euro\ ISO-8859-15
    en_US.UTF-8\ UTF-8
    en_US.ISO-8859-1\ ISO-8859-1
    en_US.ISO-8859-15\ ISO-8859-15
]

# EOF
