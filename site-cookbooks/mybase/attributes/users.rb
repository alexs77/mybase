# Sollen die standard Admin User angelegt werden?
default["mybase"][:create_admin_users] = true

# Leeren Hash der User anlegen
default["mybase"][:users] = {}

# Definition der anzulegenden Benutzer

# Syntax:
#     :local => {
#         :uid => 1000,
#         :gid => 1000,
#         :groups => %w[adm sudo users],
#         :home => "/home/local",
#         :comment => "Default local user",
#         :login_shell => "/bin/zsh",
#         :password_hash => '$6$PuqwUlU8$NXGV5n7TeutgvBnwWeC6Wa7tdDmFU/QwYrhdJKJMpxQwtemLtUILKF9ZEiu3F1QPyCb4U8wA7QESlv1vti8CN/',
#         :authorized_keys => [
#             "Zeile 1",
#             "Zeile 2"
#         ]
#     }

# Alle Attribute sind Optional.
# Falls "authorized_keys" definiert ist, kann es ein Array oder String sein.
# Inhalt wird nach ~/.ssh/authorized_keys geschrieben.
# Falls "authorized_keys" nicht definiert ist, wird die Cookbooks Datei
# "users/<name>.authorized_keys" kopiert; falls Quell-Datei nicht vorhanden,
# so ist dies KEIN Fehler.

# Zufälliges Kennwort, 36 Zeichen; https://stackoverflow.com/questions/88311/how-best-to-generate-a-random-string-in-ruby
# (36**(random_password_length-1) + rand(36**random_password_length)).to_s(36)
default["mybase"][:admin_users] = {
    "user" => {
        "uid" => 1000,
        "gid" => 1000,
        "groups" => %w[adm sudo users remote-login],
        "home" => "/home/user",
        "comment" => "Benutzer",
        "login_shell" => "/bin/zsh",
        "password_hash" => '$1$GFsW4K/4$/E3WZcfzgSgvdA2sqJf93.',
        "authorized_keys": "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQLkK1LA0HxVoC7S0alVaQqJ/lOQKxdUjboU693U2YjXwITeiD99K8tJAt1w3OB8hsob56Ebh+w90yxT8elZY1XykeWTMuGfBZJ5Xlp1V/TC2+skHo0GJUZHyCPq9ZjJVC24nhVjKHdgRohCLWPvKkK6Z4wjZ7dffOifTsiGwWC2H/f3Od3eG5QPWV8xwL5+/2wKueC6EPgdT2FgZ9EVFXhtlLfZ8MWQU+bwuQ3sN52arW2/603oAksjuRl1+8Vz4HFesVfiFLvF2w+oHsskUondoWSrZKsMGRsuZ7ovkDrTmQvmbrOOcBzTZFpAN2eudjLbSg3wxXx97zV30XsniUMh Alexander Skwar\n"
    }
}

# EOF
