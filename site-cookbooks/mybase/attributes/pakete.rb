# Pakete

# Sollen installierte zusätzliche Pakete
# *ENTFERNT* werden, so das nur "Baseline"
# vorhanden ist?
# => NOCH NICHT IMPLEMENTIERT!
default["mybase"][:packages][:remove_non_standard] = false

# Welche Paketsets sollen installiert werden?
default["mybase"][:packages][:sets] = [:standard, :extra]

# Extra "Standard" Pakete, die zu installieren sind
default["mybase"][:packages][:standard] = %w[
 make software-properties-common
 wget curl rsync
 openssl
 zsh fish bash-completion
 vim-nox
 postfix snmpd
 elinks links lynx w3m
 screen tmux
 tcpdump
 iotop htop sysstat lsof
 dmidecode smartmontools
 recode
 zip unzip
 whois
 heirloom-mailx
 sipcalc ipcalc
 imvirt open-vm-tools
 bc
 gdisk
 ethtool
 thin-provisioning-tools lvm2
 xfsprogs xfsdump
 bridge-utils vlan ifenslave net-tools cgmanager-utils cgroup-lite
 mercurial git
 language-pack-de language-pack-de-base language-pack-en language-pack-en-base
]

# Standard Desktop Pakete
default["mybase"][:packages][:desktop_common] = %w[
    account-plugin-jabber anacron android-tools-adb android-tools-adbd
    android-tools-fastboot android-tools-fsutils apg apparmor at baobab
    bc bsd-mailx bzip2 cntlm command-not-found command-not-found-data
    cron curl dia-gnome dmitry elinks exfat-fuse exfat-utils firefox
    firefox-locale-de freerdp-x11 friendly-recovery ftp gdisk gedit
    gedit-plugins gedit-source-code-browser-plugin gir1.2-zpj-0.0 git
    git-buildpackage git-doc gitk git-man gnome-tweak-tool
    gparted hamster-applet hamster-indicator
    heirloom-mailx hostname htop httpie ipcalc jpegoptim jq kazam
    keepass2 keepassx language-pack-de language-pack-de-base
    language-pack-gnome-de language-pack-gnome-de-base less lftp
    libreoffice-gnome links links2 lynx meld mercurial mlocate mmv
    mtr-tiny mutt nagios-nrpe-server ncftp netcat-openbsd nmap
    openssh-client openssh-server openssh-sftp-server p7zip p7zip-full
    p7zip-rar pidgin pidgin-guifications pidgin-libnotify pidgin-otr
    pidgin-plugin-pack pidgin-sipe pv pwgen qrencode rar realpath remmina
    remmina-plugin-nx remmina-plugin-rdp remmina-plugin-vnc rename rsync
    rsyslog screen seahorse sensible-utils shutter sipcalc ssl-cert
    strace subversion sudo telnet terminator thunderbird
    thunderbird-gnome-support thunderbird-locale-de tmux unrar unzip vim
    vim-addon-manager vim-fugitive vim-gnome vim-icinga2 vim-nox
    vim-runtime vim-scripts vino virtualbox virtualbox-dkms
    virtualbox-guest-additions-iso virtualbox-qt w3m w3m-img wdiff wget
    whois wireshark xclip xz-utils zfs-dkms zfs-doc zfsutils-linux
    zfs-zed zip zsh zshdb zsh-doc zsh-lovers zsh-static
]

default["mybase"][:packages][:extra_repos] = %w[
    google-chrome-beta sublime-text
]

# default["mybase"][:packages][:desktop] = %w[
#     ubuntu-gnome-desktop^
#     xubuntu-desktop^
#     ubuntu-mate-desktop^
#     lubuntu-desktop^
# ]

default["mybase"][:packages][:desktop] = %w[
    ubuntu-gnome-desktop
    xubuntu-desktop
    ubuntu-mate-desktop
    lubuntu-desktop
]

# Extra "Standard" Pakete, die zu installieren sind
# - kann/soll in einer node "überschrieben" werden
default["mybase"][:packages][:extra] = %w[]

# Name von "Response Files" mit preseed Antworten (Ubuntu)
# Der Name muss mit dem Paketset übereinstimmen
default["mybase"][:packages][:response_file][:desktop] = "default-x-display-manager.preseed"

