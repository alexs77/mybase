# "Laxe" Berechtigungen von Dateien und Ordnern.

# Beispiel:
# default["mybase"][:permissions] = {
#     :"/opt" => {
#         :owner => "root",
#         :group => "adm",
#         :perms => 0o775,
#         :only_platform => %w(ubuntu debian redhat centos)
#     }
# }
# Keys des Hashes sind Symbole und werden als Pfadnamen gewertet (→ :"/opt")
# Dies sind wieder Hashes, mit folgenden Keys:
# - :owner => Owner der Datei
# - :group => Gruppe der Datei
# - :perms => Berechtigungen der Datei
# - :only_platform => optional; Array mit Namen der Platformen, auf der die
#  Berechtigungen anzupassen sind.

default["mybase"][:permissions] = {
    :"/opt" => {
        :owner => "root",
        :group => "adm",
        :perms => 0o775,
        :only_platform => %w(ubuntu debian redhat centos)
    }
}

# EOF #
