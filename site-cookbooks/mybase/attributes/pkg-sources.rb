# Welches "Datum" für apt verwenden?
# Standard: Aktuelles Quartal (z.B. 2016/q3).
# Möglich: Woche: (z.B. 2016/42) oder aktuell (current)

# Automatisch bestimmen - immer das aktuelle Quartal
#default["mybase"][:aptdate] = Time.new.year.to_s + "/q" + ((Time.new.month-1)/3+1).to_s

# Fixes Datum
default["mybase"][:aptdate] = "current"

# Für "dev" - immer das aktulleste
#default["mybase"][:aptdate] = "current"

# Zu verwendendender Proxy - kann auch leer sein.
#default["mybase"][:sources][:proxy][:http] = 'http://192.168.177.23:8080'
#default["mybase"][:sources][:proxy][:https] = 'http://192.168.177.23:8080'
default["mybase"][:sources][:proxy][:http] = ''
default["mybase"][:sources][:proxy][:https] = ''

# Hosts, für die KEIN Proxy verwendet werden soll
default["mybase"][:sources][:proxy][:no] = %w[]

# EOF
