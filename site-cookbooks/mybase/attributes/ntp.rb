# Welchen NTP Server verwenden? ntp oder openntp?
default["mybase"][:ntp][:type] = "ntp"
# Adressen der NTP Server
default["mybase"][:ntp][:server] = ["127.127.1.0 iburst", "0.ch.pool.ntp.org iburst", "1.ch.pool.ntp.org iburst"]
default["mybase"][:ntp][:fudge] = ["127.127.1.0 stratum 10"]
# Andere "übliche" Werte

# EOF #
