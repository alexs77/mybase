# Partitionierung

# In /etc/chef/node.json können weitere LVs angelegt werden.
# Dazu in der Datei einfügen:

#    "ew":{ "partitioning": { "lvm": { "vgs": {
#        "system": {
#            "lvs": {
#                "name-des-logic-volumes": {
#                    "size": "234m",
#                    "filesystem": "ext4",
#                    "mount_point": {
#                        "location": "/mount-point",
#                        "options": "noatime,nodev,nosuid,usrquota,grpquota,user_xattr",
#                        "dump": 0,
#                        "pass": 2
#                    }
#                }
#            }
#        }
#    }}}}

# Um eines/mehrere der Vorgabe LVs zu überspringen, in
# der node.json die Definition durch "false" ersetzen:

#    "ew":{ "partitioning": { "lvm": { "vgs": {
#        "system": {
#            "lvs": {
#                "name-des-logic-volumes": false
#            }
#        }
#    }}}}

default["mybase"][:partitioning][:lvm][:vgs] = {
    :system => {
        :create => false, # Volume Group erzeugen?
        :lvs => {
            :home => {
                :size => '500m',
                :filesystem => 'ext4',
                :mount_point => {location: '/home', options: 'noatime,nodev,nosuid,usrquota,grpquota,user_xattr', dump: 0, pass: 2}
            },
            :opt => {
                :size => '500m',
                :filesystem => 'ext4',
                :mount_point => {location: '/opt', options: 'noatime,nodev,usrquota,grpquota,user_xattr', dump: 0, pass: 2}
            },
            :log => {
                :size => '1g',
                :filesystem => 'ext4',
                :mount_point => {location: '/var/log', options: 'noatime,nodev,nosuid,usrquota,grpquota,user_xattr', dump: 0, pass: 2}
            },
            # :root => {
            #     :size => '500m',
            #     :filesystem => 'ext4',
            #     :mount_point => {location: '/', options: 'noatime,usrquota,grpquota,user_xattr', dump: 0, pass: 1}
            # },
            :server => {
                :size => '500m',
                :filesystem => 'ext4',
                :mount_point => {location: '/srv', options: 'noatime,nodev,nosuid,usrquota,grpquota,user_xattr', dump: 0, pass: 2}
            },
            :swap => {
                :size => '4g',
                :filesystem => 'SWAP',
                :mount_point => nil,
            },
            :temp => {
                :size => '1g',
                :filesystem => 'ext4',
                :mount_point => {location: '/tmp', options: 'noatime,nodev,nosuid,usrquota,grpquota,user_xattr', dump: 0, pass: 2}
            },
            :var => {
                :size => '4g',
                :filesystem => 'ext4',
                :mount_point => {location: '/var', options: 'noatime,nodev,nosuid,usrquota,grpquota,user_xattr', dump: 0, pass: 2}
            }
        }
    }
}

# EOF #
