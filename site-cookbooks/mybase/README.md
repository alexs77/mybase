mybase Cookbook
===========

Das "*standard*" `mybase` Cookbook.

Konzept / Übersicht
-------------------

Die "Grundidee" ist, das man das Repository cloned und dann das oder die
Recipe(s) aufruft, die man konkret benötigt. Da das Cookbook einige
Abhängigkeiten hat, cloned man eher das [`mybase` Repository][mybase Repository],
welches u.a. auch dieses `ew` Repository/Cookbook clonen wird.

:exclamation: Das [`default` Recipe][default Recipe] nimmt **keinerlei** Änderungen am System
vor! Es führt nur das [`help` Recpipe][] aus, welches eine Hilfe ausgibt (u.U.
nur im Log sichtbar, sofern man nicht z.B. `chef-client` mit `-L /dev/stdout`
auf Stdout Logmeldungen ausgeben lässt). Um Änderungen am System vorzunehmen,
passe man die `run_list` nach seinen Wünschen an.

Benutzung
---------

## Setup (initial)

Sinnvollerweise cloned man das [`mybase` Repository][], da man so
auch die Abhängigkeiten mit downloadet. Bei `mybase` ist das Setup
näher beschrieben.

Attribute
---------

Die u.g. Recipes beziehen ihre Werte aus Chef Attributen. Die Default Werte
sind in Dateien im [`attributes` Ordner][] definiert. Wenn man Änderungen
durchführen möchte, die nur für die aktuelle "Node" gültig sind, so übersteuere
man die Werte in der Kopie der [`vorlage.json`][] Datei im [`nodes`][] Ordner.
Dies ist im Abschnitt [Setup (initial)][] vom [`mybase` Repository][mybase Repository]
beschrieben.

Recipes
-------

Auflistung aller Recipes, die im Cookbook definiert sind.

## [meta-default][]

Ein "Meta-Recipe", welches alle (wichtigen…) "Sub-Recipes" vom Cookbook,
passend zum OS und Platform des Clients, aufruft.

 * OS: **Alle**

## [meta-desktop][]

Ein "Meta-Recipe", um ein Desktop System zu konfigurieren.

 * OS: **Ubuntu**

## [meta-rootserver][]

Ein "Meta-Recipe", um ein Rootserver System zu konfigurieren.

 * OS: **Ubuntu**

## [default][]

Standard Recipe. Ruft `help` auf.

  * OS: **Alle**

## [help][]

Hilfetext ausgeben, ggf. nur im Log.

 * OS: **Alle**

## [misc][]

Verschiedenes. Sollte nach Möglichkeit *nichts* machen.

 * OS: **Alle**

## [test][]

---

Testing (no-op)

 * OS: **Alle**

## [audit][]

Konfiguration des auditd.

 * OS: **Linux**

## [audit-bash][]

Bash (und zsh) Auditing mit `bash_franzi` Script einrichten.

  * OS: **Alle**

## [deb-sources][]

`/etc/apt/sources.list` bzw. `/etc/apt/sources.list.d` Datei(en) einstellen.
Konfiguriert Verwendung des internen Mirrors ubuntu.1st.ch mit aktuellem
Quartal (z.B. 2017/q1).

Zugehörige Attribute: `node[:ew][:sources]` & `node[:ew][:apthost]`, siehe
[`pkg-sources` Attributes] Datei.

 * OS: **Linux**
   * Plattform: **Debian** (*Ubuntu*)

## [dns-resolve][]

DNS Resolving konfigurieren.

Zugehörige Attribute: `node['resolver']`, siehe [`resolver` Attributes] Datei.

  * OS: OpenBSD, **Centos**, Fedora, **FreeBSD**, **Debian**, **Ubuntu**, **Redhat**, Solaris, Macosx

## [fbsd-sources][]

Paketquellen konfigurieren.

Zugehörige Attribute: `node[:ew][:sources]`, siehe [`pkg-sources` Attributes] Datei.

:bangbang: Derzeit noch no-op!

  * OS: **FreeBSD**

## [hardening][]

"*Standard*" System **Hardening**.

Zugehörige Attribute: `node[:ew][:hardening]`, siehe [`hardening` Attributes] Datei.

  * OS: **Alle**

## [disable-floppy][]

Floppy Drive deaktivieren.

Zugehörige Attribute: keine

  * OS: **Linux**

## [ipa-check][]

IPA Checks.

:bangbang: Derzeit noch no-op!

 * OS: **Alle**

## [locales][]

Locales konfigurieren.

Zugehörige Attribute: `node[:ew][:locales]`, siehe [`locales` Attributes] Datei.

 * OS: **Linux**

## [logrotate][]

Grundlegende logrotate Konfiguration

Zugehörige Attribute: `node['logrotate']`, siehe [`logrotate` Attributes] Datei.

 * OS: **Alle**

## [lxc][]

LXC Container spezfische Sachen.

Deaktiviert `ntp` innerhalb eines Containers.

 * OS: **Linux**

## [monitoring][]

Icinga Monitoring
Ruft das Cookbook `icinga-client` auf.

 * OS: **Alle**

## [no-chef-client][]

Chef Client beim booten nicht starten.

 * OS: **Linux**

## [ntp][]

NTP Client einstellen.

Zugehörige Attribute: `node[:ew][:ntp]`, siehe [`ntp` Attributes] Datei.

 * OS: **Alle**

## [pakete][]

Standard Pakete installieren

Zugehörige Attribute: `node[:ew][:packages]`, siehe [`pakete` Attributes] Datei.

 * OS: **Linux**

## [partitioning][]

Standard Partitionen (bzw. LVM LVs) erstellen.

:exclamation: Hinweis: Es ist möglich, dass das Recipe nicht fehlerfrei
durchläuft, da es nicht möglich ist, bestimmte Verzeichnisse zu löschen (z.B.
bei `/var` und dem Verzeichnis `/var/log`, wenn ein eigenes Dateisystem ist,
oder bei Verzeichnissen wie `/var/lib/lxcfs`).

In diesem Falle korrigiere man das Fehlende (z.B. Eintrag in `/etc/fstab`
und mount des richtigen Devies) und rufe anschliessend das Recipe erneut
auf.

Zugehörige Attribute: `node[:ew][:partitioning]`, siehe [`partitioning` Attributes] Datei.

 * OS: **Alle**

## [permissions][]

Standard Berechtigungen von Dateien & Verzeichnissen setzen. Z.T. 
Überschneidung mit Recipe [`ew::hardening`][hardening].

Zugehörige Attribute: `node[:ew][:permissions]`, siehe [`permissions` Attributes] Datei.

 * OS: **Alle**

## [rh-sources][]

Paketquellen konfigurieren (`yum`)

Zugehörige Attribute: `node[:ew][:sources]`, siehe [`pkg-sources` Attributes] Datei.

:bangbang: Derzeit noch no-op!

 * OS: **Linux**
   * Plattform: **Redhat**

## [skel][]

`/etc/skel` Verzeichnis

 * OS: **Alle**

## [timezone][]

Timezone konfigurieren.

Zugehörige Attribute: `node["mybase"]["timezone"]`, siehe [`timezone` Attributes][timezone Attributes] Datei.

 * OS: **Alle**

## [ssh][]

SSH Daemon.

Zugehörige Attribute: `node['openssh']`, siehe [`openssh` Attributes] Datei.

 * OS: **Alle**

## [sudo][]

sudo Konfiguration.

 * OS: **Alle**

## [tmpfs][]

`/tmp` als tmpfs mounten

## [useradd-no-groups][]

useradd umkonfigurieren

 * OS: **Alle**

## [users][]

Standardnutzer anlegen. Legt auch **Gruppe** `remote-login` an.

Zugehörige Attribute: `node[:ew][:users]` & `node[:ew][:admin_users]` &
`node[:ew][:create_admin_users]`, siehe [`users` Attributes] Datei.

 * OS: **Alle**

## [vim][]

Vim konfigurieren

 * OS: **Alle**

## [zsh][]

Recipe, welches die einzig wahre™ Login Shell _RICHTIG_™^2 konfiguriert

 * OS: **Alle**


Abhängigkeiten
--------------

## Cookbooks

Folgende externe Cookbooks werden verwendet:

 - [apt][]
 - [logrotate][]
 - [openssh][]
 - [resolver][]
 - [sysctl][]

Contributing
------------

1. Einen sinnvoll benannten Feature Branch erzeugen (z.B. `add_component_x`)
2. Änderungen durchführen
3. Ggf. Testcases erstellen
  * Tests durchführen
4. Änderungen (im neuen Branch) auf Gitlab hochladen (`git commit && git push`)
5. Einen Merge Request erstellen

License and Authors
-------------------
Authors: Alexander Skwar <a@skwar.me>

[mybase Repository]: https://google.de/mybase
[`vorlage.json`]: https://google.de/mybase/blob/master/nodes/vorlage.json
[`nodes`]: https://google.de/mybase/tree/master/nodes
[Setup (initial)]: https://google.de/mybase#setup-initial

[default Recipe]: recipes/default.rb
[`help` Recpipe]: recipes/help.rb

[all]: recipes/all.rb
[meta-default]: recipes/meta-default.rb
[meta-desktop]: recipes/meta-desktop.rb
[meta-rootserver]: recipes/meta-rootserver.rb
[default]: recipes/default.rb
[help]: recipes/help.rb
[audit]: recipes/audit.rb
[audit-bash]: recipes/audit-bash.rb
[deb-sources]: recipes/deb-sources.rb
[dns-resolve]: recipes/dns-resolve.rb
[disable-floppy]: recipes/disable-floppy.rb
[fbsd-sources]: recipes/fbsd-sources.rb
[hardening]: recipes/hardening.rb
[ipa-check]: recipes/ipa-check.rb
[locales]: recipes/locales.rb
[logrotate]: recipes/logrotate.rb
[lxc]: recipes/lxc.rb
[misc]: recipes/misc.rb
[monitoring]: recipes/monitoring.rb
[no-chef-client]: recipes/no-chef-client.rb
[ntp]: recipes/ntp.rb
[pakete]: recipes/pakete.rb
[partitioning]: recipes/partitioning.rb
[permissions]: recipes/permissions.rb
[rh-sources]: recipes/rh-sources.rb
[skel]: recipes/skel.rb
[timezone]: recipes/timezone.rb
[ssh]: recipes/ssh.rb
[sudo]: recipes/sudo.rb
[test]: recipes/test.rb
[tmpfs]: recipes/tmpfs.rb
[useradd-no-groups]: recipes/useradd-no-groups.rb
[users]: recipes/users.rb
[vim]: recipes/vim.rb
[zsh]: recipes/zsh.rb

[`attributes` Ordner]: attributes/

[`hardening` Attributes]: attributes/hardening.rb
[`locales` Attributes]: attributes/locales.rb
[`logrotate` Attributes]: attributes/logrotate.rb
[`monitoring` Attributes]: attributes/monitoring.rb
[`ntp` Attributes]: attributes/ntp.rb
[`openssh` Attributes]: attributes/openssh.rb
[`pakete` Attributes]: attributes/pakete.rb
[`partitioning` Attributes]: attributes/partitioning.rb
[`permissions` Attributes]: attributes/permissions.rb
[`pkg-sources` Attributes]: attributes/pkg-sources.rb
[`resolver` Attributes]: attributes/resolver.rb
[timezone Attributes]: attributes/timezone.rb
[`users` Attributes]: attributes/users.rb

[apt]: https://supermarket.chef.io/cookbooks/apt
[logrotate]: https://supermarket.chef.io/cookbooks/logrotate
[openssh]: https://supermarket.chef.io/cookbooks/openssh
[resolver]: https://supermarket.chef.io/cookbooks/resolver
[sysctl]: https://supermarket.chef.io/cookbooks/sysctl

