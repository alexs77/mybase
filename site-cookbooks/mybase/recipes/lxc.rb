#
# Cookbook Name:: mybase
# Recipe:: lxc
#
# Copyrigt 2017, Alexander Skwar
#
# All rights reserved - Do Not Redistribute
#

# Recipe, welches Sachen macht, die mit LXC zu tun haben.
# Es wird entweder Host spezifisches ausgeführt oder Container spezifisches.

# Es ist safe, das Recipe immer drin zu haben.
# Cf. http://stackoverflow.com/questions/20010199/determining-if-a-process-runs-inside-lxc-docker

# ew::lxc-container in LXC Containern ausführen
include_recipe 'ew::lxc-container' if ::File.exist?("/dev/lxc/console")
# ew::lxc-host auf LXC Hosts ausführen, falls LXC vorhanden ist
include_recipe 'ew::lxc-host' if ::File.exist?("/var/lib/lxc") and not ::File.exist?("/dev/lxc/console")

# EOF
