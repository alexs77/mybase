#
# Cookbook Name:: mybase
# Recipe:: packages
#
# Copyrigt 2017, Alexander Skwar
#
# All rights reserved - Do Not Redistribute
#

# Recipe, welches benötige, zusätzliche Pakete installiert.

#############################################
# Pakete installieren
packages = []
responses = ""

# Pakete in Sets installieren
node[cookbook_name][:packages][:sets].each do |setname|
    package "Standard Pakete installieren. Set: " + setname.to_s + " Pakete: " + node[cookbook_name][:packages][setname].to_s do
        #package_name packages
        package_name node[cookbook_name][:packages][setname]
        action :install
        response_file "%s/%s" % [
            "pakete",
            node[cookbook_name][:packages][:response_file][setname]
        ] unless node[cookbook_name][:packages][:response_file][setname].nil?
    end # of package "Standard Pakete installieren. Set: " + setname.to_s + " Pakete: " + node[cookbook_name][:packages][setname].to_s do
end # of node[cookbook_name][:packages][:sets].each do |setname|

# EOF
