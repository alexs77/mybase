#
# Cookbook Name:: mybase
# Recipe:: monitoring
#
# Copyrigt 2017, Alexander Skwar
#
# All rights reserved - Do Not Redistribute
#

# Recipe, welches das Icinga Monitoring einrichtet

include_recipe 'icinga-client::default'
