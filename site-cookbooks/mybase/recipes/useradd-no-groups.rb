#
# Cookbook Name:: mybase
# Recipe:: misc
#
# Copyrigt 2017, Alexander Skwar
#
# All rights reserved - Do Not Redistribute
#

# Recipe, welches useradd "umkonfiguriert", so das keine Gruppen erzeugt werden.

#############################################
# useradd soll keine Gruppen erzeugen
bash "login.defs - useradd no groups" do
    user "root"
    code <<-EOlogin_defs_USERGROUPS_ENAB
        sed -i 's,USERGROUPS_ENAB yes,USERGROUPS_ENAB no,' /etc/login.defs
    EOlogin_defs_USERGROUPS_ENAB
    not_if {::File.foreach("/etc/login.defs").grep(/^USERGROUPS_ENAB no/).any?}
end

# EOF
