#
# Cookbook Name:: mybase
# Recipe:: ipa-check
#
# Copyrigt 2017, Alexander Skwar
#
# All rights reserved - Do Not Redistribute
#

# Überprüfe, ob das System in IPA eingebunden ist.
# Wenn *NICHT*, dann werden *ALLE* recipes aus der
# run-list entfernt, die auf -ipa enden.

# https://docs.chef.io/resource_common.html#only-if-examples
# → Remove a recipe if it belongs to a specific run-list

# Irgendwie geht das nicht so ganz…
# Eigentlich hätte node.run_list.expand klappen sollen, aber da ist
# die Liste der run_list_items dann leer.

# Idee:
# - Die run_list "expanden". Also Rollen in recipes "auflösen"
# - Bei allen recipes schauen, ob der Name auf -ipa endet
# - Wenn ja: Löschen, sofern IPA noch nicht initialisiert wurde.

# Stand: 2015-09-29, ask

# EOF
