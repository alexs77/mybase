#
# Cookbook Name:: mybase
# Recipe:: hardening-linux
#
# Copyrigt 2017, Alexander Skwar
#
# All rights reserved - Do Not Redistribute
#

# Hardening des Systems - Linux

#############################################
# getty reduzieren
for tty_number in 3..6
    tty="tty" + tty_number.to_s

    service "Stoppy getty für " + tty do
        service_name tty
        action :stop
        only_if { node['init_package'] == "upstart" && ::File.exist?("/etc/init/" + tty + ".conf") }
    end # of service "Stoppy getty für " + tty do

    file "Getty upstart Job entfernen: " + tty do
        path "/etc/init/" + tty + ".conf"
        action :delete
        only_if { node['init_package'] == "upstart" && ::File.exist?("/etc/init/" + tty + ".conf") }
    end # of file "Getty upstart Job entfernen: " + tty do
end # of for tty_number in 3..6

#############################################
# CIS: AutoFS deaktivieren
service "CIS: AutoFS Dienst stoppen" do
    service_name "autofs"
    action :stop
    only_if {::File.exist?("/etc/init/autofs.conf")}
end # of service "CIS: AutoFS Dienst stoppen" do

ruby_block "CIS: AutoFS start deaktivieren" do
    block do
        fe = Chef::Util::FileEdit.new("/etc/init/autofs.conf")
        # "start on" auskommentieren
        fe.search_file_replace_line(/^(start on.*)/,
            "# removed because of CIS check:\n# \1"
            )

        # Datei schreiben
        fe.write_file
    end # of block do
    only_if {::File.exist?("/etc/init/autofs.conf")}
end # of ruby_block "CIS: AutoFS start deaktivieren" do

#############################################
# CIS: /run/shm mount Optionen
mount "CIS: /run/shm Mount-Optionen anpassen" do
    device "none"
    mount_point "/run/shm"
    fstype "tmpfs"
    options %w[rw nosuid nodev relatime noexec]
    dump 0
    pass 0
    enabled true
    supports [:remount]
    action [:enable, :remount]
    not_if { ::File.exists?("/dev/lxc/console") }
end # of mount "CIS: /run/shm Mount-Optionen anpassen" do

#############################################
# CIS: Kernel Modul Support entfernen
remote_directory "CIS: Kernel Modul Support entfernen" do
    source 'hardening/modprobe.d'
    path '/etc/modprobe.d'

    owner 'root'
    group 'root'
    mode 0o755

    files_owner 'root'
    files_group 'root'
    files_mode 0o644

    action :create
end # of remote_directory "CIS: Kernel Modul Support entfernen" do

# EOF
