#
# Cookbook Name:: mybase
# Recipe:: partitioning-linux
#
# Copyrigt 2017, Alexander Skwar
#
# All rights reserved - Do Not Redistribute
#

# Recipe, welches die Standard "Partitionen" auf dem System erzeugt.

# Siehe Attributes Datei für die definierten LVs und wie die
# zu "übersteuern" wären.

node[cookbook_name][:partitioning][:lvm][:vgs].each do |vgname, vgdef|
    # Überspringen, falls es das LVM Executable nicht gibt.
    next unless ::File.executable?( "/sbin/lvm" )

    vgdef[:lvs].each do |lvname, lvdef|
        # Gibt es eine Definition in lvdef?
        # Wenn nein (bzw. == false), dann überspringen.
        next if lvdef == false

        # LV evtl. erzeugen.
        # Wenn das ausgeführt wurde (weil LV noch nicht vorhanden),
        # dann:
        # - (01) Dateisystem erzeugen
        # - (02) Erzeugen: Verzeichnis für richtigen LV Mount (sofern noch nicht da => "not_if")
        # - (03) Erzeugen: Verzeichnis für temp. LV Mount
        # - (04) Mount LV dort
        # - (05) Erzeugen: Verzeichnis für temp. Bind mount
        # - (06) Mount Bind dort
        # - (07) Kopieren von Bind => LV
        # - (08) Löschen von Daten auf Bind
        # - (09) Umount Bind
        # - (10) Löschen: Verzeichnis für temp. Bind mount
        # - (11) Umount LV
        # - (12) Löschen: Verzeichnis für temp. LV Mount
        # - (13) Mount LV an richtigem Ort
        # - (14) Mount LV in fstab eintragen

        # Swap LV evtl. erzeugen.
        # Wenn das ausgeführt wurde (weil LV noch nicht vorhanden),
        # dann:
        # - (01) Swap "Dateisystem" erzeugen
        # - (02) Swap nutzen
        # - (03) Swap in fstab eintragen

        # Alte mount options bestimmen und übernehmen, falls vorhanden
        mount_point = {}
        if lvdef[:mount_point].is_a?(Hash) then
            # Wurden *KEINE* "options" angegeben?
            unless lvdef[:mount_point].has_key?("options") or lvdef[:mount_point].has_key?(:options) then
                # => übernehme alte Options (sofern vorhanden)
                if node["filesystem"].has_key?(lv_mapper_dev) then
                    mount_point_options = node["filesystem"][lv_mapper_dev]["mount_options"].join(",")
                end # of if node["filesystem"].has_key?(lv_mapper_dev) then
            else
                # => übernahme der angegebenen Options
                mount_point[:options] = lvdef[:mount_point][:options]
            end # of if lvdef[:mount_point].has_key?("options") or lvdef[:mount_point].has_key?(:options) then

            # Übernahme von den anderen Keys
            [:location, :dump, :pass].each do |name|
                mount_point[name] = lvdef[:mount_point][name] if lvdef[:mount_point].has_key?(name)
            end # of [:location, :dump, :pass].each do |name|
        else
            # Benutzer hat bei mount_point kein Hash angegeben, also nur einen
            # String, der für die "location" steht
            mount_point[:location] = lvdef[:mount_point]
        end # of if lvdef[:mount_point].is_a?(Hash) then

        # Definiere einiger "Hilfsvariablen"
        # Name der Volume Group
        # → Bereits durch "each #1" definiert
        #vgname = "system"
        # Name des Logical Volumes
        # → Bereits durch "each #2" definiert
        #lvname = "test-data"
        # Name des LVM Devices (/dev/system/test-data → /dev/mapper/system-test--data)
        lv_mapper_dev = "/dev/mapper/#{vgname.gsub("-", "--")}-#{lvname.gsub("-", "--")}"
        # richtiger Mountpoint des Logical Volumes
        # Auch: "Quelle"/"Device" für Bind Mount
        lv_right_mount_point_dir = lvdef[:mount_point][:location] unless lvdef[:mount_point].nil?
        #lv_right_mount_point_dir = mount_point[:location]
        # temporärer Mountpoint des Logical Volumes
        # Auch: Verzeichnis, IN DAS Daten zu verschieben sind
        lv_temp_mount_point_dir = "/.lvm-migration/ziel.#{mount_point[:location].tr("/", ":")}" unless mount_point[:location].nil?
        # Name des Verzeichnisses, das als Bind Mount Ziel dient.
        # Auch: "altes" Verzeichnis, VON DEM Daten zu verschieben sind
        old_dir_bind_mount_dir = "/.lvm-migration/quelle.#{mount_point[:location].tr("/", ":")}" unless mount_point[:location].nil?

        # Dateisystem erzeugen
        execute "Dateisystem #{lvdef[:filesystem]} erzeugen auf #{lv_mapper_dev}" do
            command "/sbin/mkfs -t '#{lvdef[:filesystem]}' '#{lv_mapper_dev}'"
            action :nothing
        end # of execute "Dateisystem #{lvdef[:filesystem]} erzeugen auf #{lv_mapper_dev}" do

        # temporärer Mountpoint des Logical Volumes (Verzeichnis)
        directory "temporärer Mountpoint des Logical Volumes: #{lv_temp_mount_point_dir}" do
            path lv_temp_mount_point_dir
            mode 0o000
            action :nothing
            recursive true
        end # of directory "temporärer Mountpoint des Logical Volumes: #{lv_temp_mount_point_dir}" do

        # temp. Mount von LVM Dev
        mount "temp. Mount von LVM Dev: #{lv_mapper_dev} @ #{lv_temp_mount_point_dir}" do
            mount_point lv_temp_mount_point_dir
            device lv_mapper_dev
            action :nothing
        end # of mount "temp. Mount von LVM Dev: #{lv_mapper_dev} @ #{lv_temp_mount_point_dir}" do

        # Daten aus Bind Mount nach LVM Temp Mount kopieren
        execute "LVM Kopie Temp. Verzeichnis Inhalte kopieren: #{old_dir_bind_mount_dir} → #{lv_temp_mount_point_dir}" do
            command "cp -rp -- '#{old_dir_bind_mount_dir}/.' '#{lv_temp_mount_point_dir}/'"

            # Fehler sind wichtig!
            ignore_failure false

            # Muss als root laufen.
            user "root"
            action :nothing

            # Wenn erfolgreich, dann auch löschen
            #notifies :run, "execute[Daten in Bind Mount löschen: #{old_dir_bind_mount_dir}]", :immediately

            # Überprüfe, ob Quelle und Ziel *UNTERSCHIEDLICH* sind.
            # "Unterschiedlich" := Inodes unterschiedlich UND Devices unterschiedlich
            not_if { (File.stat("#{old_dir_bind_mount_dir}/.").ino == File.stat("#{lv_temp_mount_point_dir}/.").ino) && (File.stat("#{old_dir_bind_mount_dir}/.").dev == File.stat("#{lv_temp_mount_point_dir}/.").dev) }
        end # of execute "LVM Kopie Temp. Verzeichnis Inhalte kopieren: #{old_dir_bind_mount_dir} → #{lv_temp_mount_point_dir}" do

        # Daten in Bind Mount löschen
        execute "Daten in Bind Mount löschen: #{old_dir_bind_mount_dir}" do
            command "find '#{old_dir_bind_mount_dir}' -mindepth 1 -delete"

            # Fehler sind wichtig!
            ignore_failure false

            # Muss als root laufen.
            user "root"
            action :nothing
        end # of execute "Daten in Bind Mount löschen: #{old_dir_bind_mount_dir}" do

        # Name des Verzeichnisses, das als Bind Mount *Ziel* dient
        directory "LVM Move Quelle: #{old_dir_bind_mount_dir}" do
            path old_dir_bind_mount_dir
            mode 0o000
            action :nothing
            recursive true
        end # of directory "LVM Move Quelle: #{old_dir_bind_mount_dir}" do

        # Bind Mount von Original Verzeichnis auf Verz. definieren
        # Aus dem Bind Mount wird verschoben werden, NICHT aus dem
        # echten Verzeichnis.
        # Grund: Wenn es "sub-mounts" gibt (z.B. /var & /var/log), so würde
        # bei einem mv /var/* auch /var/log incl. Daten verschoben werden. Das
        # will man nicht.
        mount "Bind Mount: #{lv_right_mount_point_dir} => #{old_dir_bind_mount_dir}" do
            mount_point old_dir_bind_mount_dir
            device lv_right_mount_point_dir
            fstype "none"
            options "bind,rw"
            action :nothing
        end # of mount "Bind Mount: #{lv_right_mount_point_dir} => #{old_dir_bind_mount_dir}" do
        # "mount" Resource kann bind mount nicht aushängen
        # Siehe z.B. https://github.com/chef/chef/issues/4660
        execute "Bind Mount umount: #{lv_right_mount_point_dir} => #{old_dir_bind_mount_dir}" do
            command "umount #{old_dir_bind_mount_dir}"
            user "root"
            action :nothing
        end # of execute "Bind Mount umount: #{lv_right_mount_point_dir} => #{old_dir_bind_mount_dir}" do

        # Verzeichnis für richtiger Mountpoint des Logical Volumes
        directory "richtiger Mountpoint des Logical Volumes: #{lv_right_mount_point_dir}" do
            path lv_right_mount_point_dir
            mode 0o000
            action :nothing

            not_if { ::File.exists?(lv_right_mount_point_dir) }
        end # of directory "richtiger Mountpoint des Logical Volumes: #{lv_right_mount_point_dir}" do

        # richtiger Mountpoint des Logical Volumes
        mount "richtiger Mountpoint des Logical Volumes: #{lv_right_mount_point_dir} @ #{lv_mapper_dev}" do
            device lv_mapper_dev
            mount_point lv_right_mount_point_dir
            options mount_point[:options]
            action :nothing
        end # of mount "richtiger Mountpoint des Logical Volumes: #{lv_right_mount_point_dir} @ #{lv_mapper_dev}" do

        # Swap Dateisystem erzeugen
        execute "Swap Dateisystem erzeugen auf #{lv_mapper_dev}" do
            command "/sbin/mkswap --force '#{lv_mapper_dev}'"
            action :nothing
        end # of execute "Swap Dateisystem erzeugen auf #{lv_mapper_dev}" do

        # Swap nutzen
        execute "Swap nutzen auf #{lv_mapper_dev}" do
            command "/sbin/swapon #{lv_mapper_dev}"
            action :nothing
        end # of execute "Swap nutzen auf #{lv_mapper_dev}" do

        # Swap Mountpoint des Logical Volumes
        mount "Swap Mountpoint des Logical Volumes: #{lv_mapper_dev}" do
            device lv_mapper_dev
            mount_point 'none'
            fstype 'swap'
            options 'sw'
            dump 0
            pass 0
            action :nothing
        end # of mount "Swap Mountpoint des Logical Volumes: #{lv_mapper_dev}" do

        # Logical Volume erzeugen
        execute "LV erzeugen #{lv_mapper_dev}" do
            command "/sbin/lvm lvcreate --size '#{lvdef[:size]}' --name '#{lvname}' -y -Wy -Zy '#{vgname}'"
            user "root"

            # Nur für *NICHT* Swap Filesystems was machen.
            not_if { lvdef[:filesystem].casecmp("swap").zero? }
            #not_if { lvdef[:filesystem].downcase == "swap".downcase }

            # Alles nur dann machen, wenn es das LV noch *NICHT* gibt
            not_if { ::File.exists?(lv_mapper_dev) }
            #not_if { ::File.exists?("/dev/#{vgname}/#{lvname}") }
            #not_if "/sbin/lvm lvs /dev/#{vgname}/#{lvname} > /dev/null 2>&1"

            #### LVM
            # - (01) Dateisystem erzeugen
            notifies :run, "execute[Dateisystem #{lvdef[:filesystem]} erzeugen auf #{lv_mapper_dev}]", :immediately
            # - (02) Erzeugen: Verzeichnis für richtigen LV Mount (sofern noch nicht da => "not_if")
            notifies :create, "directory[richtiger Mountpoint des Logical Volumes: #{lv_right_mount_point_dir}]", :immediately
            # - (03) Erzeugen: Verzeichnis für temp. LV Mount
            notifies :create, "directory[temporärer Mountpoint des Logical Volumes: #{lv_temp_mount_point_dir}]", :immediately
            # - (04) Mount LV dort
            notifies :mount, "mount[temp. Mount von LVM Dev: #{lv_mapper_dev} @ #{lv_temp_mount_point_dir}]", :immediately

            ### Bind Mount
            # - (05) Erzeugen: Verzeichnis für temp. Bind mount
            notifies :create, "directory[LVM Move Quelle: #{old_dir_bind_mount_dir}]", :immediately
            # - (06) Mount Bind dort
            notifies :mount, "mount[Bind Mount: #{lv_right_mount_point_dir} => #{old_dir_bind_mount_dir}]", :immediately

            ### Daten migrieren
            # - (07) Kopieren von Bind => LV
            notifies :run, "execute[LVM Kopie Temp. Verzeichnis Inhalte kopieren: #{old_dir_bind_mount_dir} → #{lv_temp_mount_point_dir}]", :immediately
            # - (08) Löschen von Daten auf Bind
            notifies :run, "execute[Daten in Bind Mount löschen: #{old_dir_bind_mount_dir}]", :immediately

            ### Bind Mount
            # - (09) Umount Bind
            notifies :run, "execute[Bind Mount umount: #{lv_right_mount_point_dir} => #{old_dir_bind_mount_dir}]", :immediately
            # - (10) Löschen: Verzeichnis für temp. Bind mount
            notifies :delete, "directory[LVM Move Quelle: #{old_dir_bind_mount_dir}]", :immediately

            ### LVM
            # - (11) Umount LV
            notifies :umount, "mount[temp. Mount von LVM Dev: #{lv_mapper_dev} @ #{lv_temp_mount_point_dir}]", :immediately
            # - (12) Löschen: Verzeichnis für temp. LV Mount
            notifies :delete, "directory[temporärer Mountpoint des Logical Volumes: #{lv_temp_mount_point_dir}]", :immediately
            # - (13) Mount LV an richtigem Ort
            notifies :mount, "mount[richtiger Mountpoint des Logical Volumes: #{lv_right_mount_point_dir} @ #{lv_mapper_dev}]", :immediately
            # - (14) Mount LV in fstab eintragen
            notifies :enable, "mount[richtiger Mountpoint des Logical Volumes: #{lv_right_mount_point_dir} @ #{lv_mapper_dev}]", :immediately
        end # of execute "LV erzeugen #{lv_mapper_dev}" do

        # Logical Volume Swap erzeugen
        execute "Swap LV erzeugen #{lv_mapper_dev}" do
            command "/sbin/lvm lvcreate --size '#{lvdef[:size]}' --name '#{lvname}' -y -Wy -Zy '#{vgname}'"
            user "root"

            # Nur für Swap Filesystems was machen.
            only_if { lvdef[:filesystem].casecmp("swap").zero? }

            # Alles nur dann machen, wenn es das LV noch *NICHT* gibt
            not_if { ::File.exists?(lv_mapper_dev) }

            # - (01) Swap "Dateisystem" erzeugen
            notifies :run, "execute[Swap Dateisystem erzeugen auf #{lv_mapper_dev}]", :immediately
            # - (02) Swap nutzen
            notifies :run, "execute[Swap nutzen auf #{lv_mapper_dev}]", :immediately
            # - (03) Swap in fstab eintragen
            notifies :enable, "mount[Swap Mountpoint des Logical Volumes: #{lv_mapper_dev}]", :immediately
        end # of execute "Swap LV erzeugen #{lv_mapper_dev}" do
    end # of vgdef[:lvs].each do |lvname, lvdef|
end # of node[cookbook_name][:partitioning][:lvm][:vgs].each do |vgname, vgdef|

# EOF
