#
# Cookbook Name:: mybase
# Recipe:: lxc-host
#
# Copyrigt 2017, Alexander Skwar
#
# All rights reserved - Do Not Redistribute
#

# Recipe, welches Sachen macht, die NUR auf einem LXC Host zu tun sind.
# Es ist safe, das Recipe immer drin zu haben.
# Cf. http://stackoverflow.com/questions/20010199/determining-if-a-process-runs-inside-lxc-docker

# http://stackoverflow.com/a/21632901
ruby_block "lets disable the lxc bridge" do
    block do
        sed = Chef::Util::FileEdit.new("/etc/default/lxc-net")
        sed.search_file_replace(/"true/, '"false')
        sed.write_file
    end
    only_if { ::File.readlines("/etc/default/lxc-net").grep(/"true/).any? }
end # of ruby_block "lets disable the lxc bridge" do

# EOF
