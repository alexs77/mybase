#
# Cookbook Name:: mybase
# Recipe:: ntp
#
# Copyrigt 2017, Alexander Skwar
#
# All rights reserved - Do Not Redistribute
#

# Recipe, welches NTP (ntpd oder openntpd) installiert und konfiguriert.

# NTP Paket installieren
package "NTP Paket installieren: " + node[cookbook_name][:ntp][:type] do
    package_name node[cookbook_name][:ntp][:type]
    action :install
end # of package "NTP Paket installieren: " + node[cookbook_name][:ntp][:type] do

# Default Dateien nur bei ntp (nicht bei openntpd) auf Ubuntu
cookbook_file "ntp default Datei: /etc/default/ntp, Source: ntp/default." + node['lsb']['codename'] do
    path "/etc/default/ntp"
    source "ntp/default/ntp." + node['lsb']['codename']
    mode 0o644
    owner "root"
    group node[:root_group]

    only_if { node[:platform] == "ubuntu" }
    not_if { node[cookbook_name][:ntp][:type] != "ntp" }
end # cookbook_file "ntp default Datei: /etc/default/ntp, Source: ntp/default." + node['lsb']['codename'] do

cookbook_file "ntpdate default Datei (trusty): /etc/default/ntpdate" do
    path "/etc/default/ntp"
    source "ntp/default/ntp." + node['lsb']['codename']
    mode 0o644
    owner "root"
    group node[:root_group]

    only_if { node[:lsb][:codename] == "trusty" }
    not_if { node[cookbook_name][:ntp][:type] != "ntp" }
end # of cookbook_file "ntpdate default Datei (trusty): /etc/default/ntpdate" do

template "ntpd configuration file, servers: '" + node[cookbook_name][:ntp][:server].to_s + "'" do
    path case node[cookbook_name][:ntp][:type]
    when 'ntp'
        '/etc/ntp.conf'
    when 'openntpd'
        '/etc/openntpd/ntpd.conf'
    end

    source "ntp/ntp.conf.erb"
    variables({
        :ntp_server => node[cookbook_name][:ntp][:server],
        :fudges => node[cookbook_name][:ntp][:fudge]
    })
    mode 0o644
    owner "root"
    group node[:root_group]
end # of template "ntpd configuration file, servers: '" + node[cookbook_name][:ntp][:server].to_s + "'" do

service "ntp daemon" do
    service_name case node[cookbook_name][:ntp][:type]
    when 'ntp'
        'ntp'
    when 'openntp'
        'openntpd'
    end

    supports :status => true, :restart => true, :reload => true

    action [ :enable, :stop, :start ]
end # of service "ntp daemon" do

# EOF
