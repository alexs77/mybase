#
# Cookbook Name:: mybase
# Recipe:: permissions
#
# Copyrigt 2017, Alexander Skwar
#
# All rights reserved - Do Not Redistribute
#

# Recipe, welches "laxe" Berechtigungen von Dateien und Verzeichnissen setzt.
# Zum Teil auch durch "ew::hardening" erledigt.

#############################################

# System File Permissions
node[cookbook_name][:permissions].each do |filename, filedef|
    file "System File Permissions anpassen: " + filename + " -> Owner: " + filedef[:user].to_s + ":" + filedef[:group].to_s + " Rechte: " + filedef[:perms].to_s do
        path filename
        owner filedef[:owner]
        group filedef[:group]
        mode filedef[:perms]
        # Nur was ändern, wenn es die Datei gibt.
        only_if { ::File.exists?(filename) }
        # Nur was ändern, wenn es eine "normale" DATEI ist.
        only_if { ::File.file?(filename)}
        # Nur was ändern, falls :only_platform GAR NICHT gesetzt ist, bzw.
        # FALLS gesetzt, dann überprüfen, ob die akt. Platform in dem Array
        # enthalten ist.
        only_if { !filedef.has_key?(:only_platform) || filedef.has_key?(:only_platform) && filedef[:only_platform].include?(node[:platform]) }
    end # of file "System File Permissions anpassen: " + filename + " -> Owner: " + filedef[:user].to_s + ":" + filedef[:group].to_s + " Rechte: " + filedef[:perms].to_s do

    directory "System Directory Permissions anpassen: " + filename + " -> Owner: " + filedef[:user].to_s + ":" + filedef[:group].to_s + " Rechte: " + filedef[:perms].to_s do
        path filename
        owner filedef[:owner]
        group filedef[:group]
        mode filedef[:perms]
        # Nur was ändern, wenn es die Datei gibt.
        only_if { ::File.exists?(filename) }
        # Nur was ändern, wenn es ein VERZEICHNIS ist.
        only_if { ::File.directory?(filename)}
        # Nur was ändern, falls :only_platform GAR NICHT gesetzt ist, bzw.
        # FALLS gesetzt, dann überprüfen, ob die akt. Platform in dem Array
        # enthalten ist.
        only_if { !filedef.has_key?(:only_platform) || filedef.has_key?(:only_platform) && filedef[:only_platform].include?(node[:platform]) }
    end # of directory "System Directory Permissions anpassen: " + filename + " -> Owner: " + filedef[:user].to_s + ":" + filedef[:group].to_s + " Rechte: " + filedef[:perms].to_s do
end # of node[cookbook_name][:hardening][:file_permissions].each do |filename, filedef|


# EOF
