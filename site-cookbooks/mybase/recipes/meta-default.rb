#
# Cookbook Name:: mybase
# Recipe:: meta-default
#
# Copyright 2017, Alexander Skwar
#
# All rights reserved - Do Not Redistribute
#

# Recipe, welches ein System als standard System konfiguriert

# Es werden alle (wichtigen…) "Sub-Recipes" vom Cookbook "ew" aufgerufen.

#############################################
# Wichtige grundlegende Basics!

# Zuerst sudo einrichten, damit Zugang nicht verloren geht
include_recipe "#{cookbook_name}::sudo"

# Von der Platform abhängige Recipes
case node[:platform]
    when 'ubuntu', 'debian'
        # Weitere Paketquellen einbinden
        include_recipe "#{cookbook_name}::deb-sources"
    when 'redhat', 'centos'
        # Weitere Paketquellen einbinden
        include_recipe "#{cookbook_name}::rh-sources"
    when 'freebsd'
        # Weitere Paketquellen einbinden
        include_recipe "#{cookbook_name}::fbsd-sources"
end # of case node[:platform]

# Vom OS abhängige Recipes
case node[:os]
    when 'linux'
        # ew::pakete erst NACH ew::*-sources aufrufen. Und sehr früh
        # aufrufen, damit alles benötigte da ist.
        include_recipe "#{cookbook_name}::pakete"
        include_recipe "#{cookbook_name}::tmpfs"
        include_recipe "#{cookbook_name}::rsyslog"
        # include_recipe "#{cookbook_name}::partitioning" unless ::File.exists?("/dev/lxc/console")
        include_recipe "#{cookbook_name}::audit"
        include_recipe "#{cookbook_name}::locales"
        include_recipe "#{cookbook_name}::lxc"
        include_recipe "#{cookbook_name}::no-chef-client"
        include_recipe "#{cookbook_name}::useradd-no-groups"
end # of case node[:os]

# Unabhängige Recipes
include_recipe "#{cookbook_name}::timezone"
include_recipe "#{cookbook_name}::sysctl"
include_recipe "#{cookbook_name}::logrotate"
include_recipe "#{cookbook_name}::audit-bash"
include_recipe "#{cookbook_name}::dns-resolve"
#include_recipe "#{cookbook_name}::monitoring"
include_recipe "#{cookbook_name}::ntp"
include_recipe "#{cookbook_name}::skel"
include_recipe "#{cookbook_name}::ssh"
include_recipe "#{cookbook_name}::vim"
include_recipe "#{cookbook_name}::zsh"
# "ew::permissions" *VOR* "ew::hardening" aufrufen, da "ew::hardening" strikter ist.
include_recipe "#{cookbook_name}::permissions"

# Benutzer ziemlich am Schluss anlegen, damit Änderungen bei /etc/skel greifen
include_recipe "#{cookbook_name}::users"

# Hardening am Schluss
include_recipe "#{cookbook_name}::hardening"

include_recipe "#{cookbook_name}::misc"

# EOF
