#
# Cookbook Name:: mybase
# Recipe:: audit-bash
#
# Copyrigt 2017, Alexander Skwar
#
# All rights reserved - Do Not Redistribute
#

# Recipe, welches das "Franzi Script" bash_franzi, für auditing, installiert

cookbook_file 'audit-bash: forcecommand' do
    source 'audit-bash/forcecommand.sh'
    path '/etc/forcecommand.sh'
    mode 0o755
    owner 'root'
    group node[:root_group]
end # of cookbook_file 'audit-bash: forcecommand' do

# Kopiert von OpenSSH Cookbook
service_provider = nil
if 'ubuntu' == node['platform']
    service_provider = Chef::Provider::Service::Upstart
    service_provider = Chef::Provider::Service::Systemd if node['init_package'] == 'systemd' 
end
service 'audit-bash: rsyslog' do
    service_name 'rsyslog'
    supports :status => true, :restart => true, :reload => false
    action :nothing
    #provider Chef::Provider::Service::Upstart if (node.has_key?('init_package') && node['init_package'] == 'upstart')
    provider service_provider
end # of service 'audit-bash: rsyslog' do

cookbook_file 'audit-bash: rsyslog Konfiguration' do
    source 'audit-bash/rsyslog.conf'
    path '/etc/rsyslog.d/60-audit-bash.conf'
    mode 0o644
    owner 'root'
    group node[:root_group]

    notifies :restart, 'service[audit-bash: rsyslog]', :delayed
end # of cookbook_file 'audit-bash: rsyslog Konfiguration' do

cookbook_file 'audit-bash: bash_franzi' do
    source 'audit-bash/bash_franzi.sh'
    path '/etc/bash_franzi'
    mode 0o644
    owner 'root'
    group node[:root_group]
end # of cookbook_file 'audit-bash: bash_franzi' do

bash 'audit-bash: Lade bash_franzi in bashrc' do
    user 'root'
    code <<-EOH
        (
            echo ''
            echo '# audit-bash / bash_franzi'
            echo ''
            echo '[[ -r /etc/bash_franzi ]] && . /etc/bash_franzi'
        ) >> /etc/bash.bashrc
    EOH

    not_if {::File.foreach("/etc/bash.bashrc").grep(/^# audit-bash \/ bash_franzi/).any?}
end # of bash 'audit-bash: Lade bash_franzi in bashrc' do

# EOF
