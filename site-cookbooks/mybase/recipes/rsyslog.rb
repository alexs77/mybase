#
# Cookbook Name:: mybase
# Recipe:: rsyslog
#
# Copyright 2016-2017, Alexander Skwar
#
# All rights reserved - Do Not Redistribute
#

service 'rsyslog' do
    service_name 'rsyslog'
    action :nothing
end # of service 'rsyslog' do

# disable filtering of duplicated messages
file '/etc/rsyslog.d/00-RepeatedMsgReduction.conf' do
    content '$RepeatedMsgReduction off'
    mode '0644'
    owner 'root'
    group 'root'
    notifies :restart, 'service[rsyslog]', :delayed
end # of disable filtering of duplicated messages
