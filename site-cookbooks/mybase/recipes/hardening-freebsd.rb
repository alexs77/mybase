#
# Cookbook Name:: mybase
# Recipe:: hardening-freebsd
#
# Copyrigt 2017, Alexander Skwar
#
# All rights reserved - Do Not Redistribute
#

# Hardening des Systems - FreeBSD

template '/etc/syslog.conf' do
    source 'hardening/syslog.conf.erb'
    mode 0o644
    owner 'root'
    group 'wheel'
    action :create
    variables(
        :syslogserver => node[cookbook_name][:hardening][:syslogserver]
    )
end

cookbook_file '/etc/newsyslog.conf' do
    source 'hardening/newsyslog.conf'
    mode 0o644
    owner 'root'
    group 'wheel'
end

service 'syslogd' do
    action [:enable, :start]
end

# EOF
