#
# Cookbook Name:: mybase
# Recipe:: default
#
# Copyright 2017, Alexander Skwar
#
# All rights reserved - Do Not Redistribute
#

# Per default wird *NICHTS* gemacht, ausser eine Hilfe auszugeben.
# Der Admin soll auswählen, was passiert.

include_recipe "#{cookbook_name}::help"

# EOF
