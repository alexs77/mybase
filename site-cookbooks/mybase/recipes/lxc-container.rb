#
# Cookbook Name:: mybase
# Recipe:: lxc-contaner
#
# Copyrigt 2017, Alexander Skwar
#
# All rights reserved - Do Not Redistribute
#

# Recipe, welches Sachen macht, die NUR in einem LXC Container zu tun sind.
# Es ist safe, das Recipe immer drin zu haben.
# Cf. http://stackoverflow.com/questions/20010199/determining-if-a-process-runs-inside-lxc-docker

# In einem LXC Container wird der NTP Dienst nicht benötigt.
service "ntp" do
    supports :status => true, :restart => true, :reload => true
    action [ :disable, :stop ]
end # of service "ntp" do

# EOF
