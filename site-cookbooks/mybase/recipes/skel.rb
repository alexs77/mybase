#
# Cookbook Name:: mybase
# Recipe:: skel
#
# Copyrigt 2017, Alexander Skwar
#
# All rights reserved - Do Not Redistribute
#

# Lege "Standard-Dateien" in /etc/skel an
# und auch bei /root und /home/local, sofern nicht existent
#############################################
# /etc/skel Dateien
%w[zshrc shrc bashrc profile sh_aliases].each do |skelfile|
    cookbook_file "skel Datei: " + skelfile do
        source "skel/" + skelfile
        path "/etc/skel/." + skelfile
        mode 0o644
        owner "root"
        group node[:root_group]
    end # of cookbook_file "skel Datei: " + skelfile do

    %w[root local pxe-local].each do |username|
        # Überprüfen, ob der User $username vorhanden ist - wenn nicht, 
        # springe zum nächsten Durchlauf.
        next unless node[:etc][:passwd].attribute?(username)

        cookbook_file "skel Datei für User " + username + " in homedir " + ::File.expand_path("~" + username) + ": " + skelfile do
            source "skel/" + skelfile
            path ::File.expand_path("~" + username + "/." + skelfile)
            mode 0o644
            owner ::File.stat(::File.expand_path("~" + username)).uid
            group ::File.stat(::File.expand_path("~" + username)).gid
            #not_if { ::File.exist?(::File.expand_path("~" + username + "/." + skelfile)) }
            action :create_if_missing
        end # of cookbook_file "skel Datei für User " + username + " in homedir " + ::File.expand_path("~" + username) + ": " + skelfile do
    end # of %w[root local pxe-local].each do |username|
end # of %w[zshrc shrc bashrc profile sh_aliases].each do |skelfile|

# EOF
