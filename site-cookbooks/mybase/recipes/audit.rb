#
# Cookbook Name:: mybase
# Recipe:: audit
#
# Copyrigt 2017, Alexander Skwar
#
# All rights reserved - Do Not Redistribute
#

# Recipe, welches den audit Daemon konfiguriert.

audit_pkgs = %w[auditd audispd-plugins]
package "auditd Pakete installieren: " + audit_pkgs.to_s do
    package_name audit_pkgs
    action :install
end # of package "auditd Pakete installieren: " + audit_pkgs.to_s do

# Wenn systemd benutzt wird (xenial, Debian 8, Redhat,…), dann:
if node['init_package'] == 'systemd'
    execute 'systemctl daemon-reload' do
        command 'systemctl daemon-reload'
        action :nothing
    end # of execute 'systemctl daemon-reload' do

    directory '/etc/systemd/system/auditd.service.d' do
        path '/etc/systemd/system/auditd.service.d'
        mode '0755'
        owner 'root'
        group 'root'
        action :create
    end # of directory '/etc/systemd/system/auditd.service.d' do

    cookbook_file "/etc/systemd/system/auditd.service.d/override.conf" do
        source 'auditd/auditd.systemd.service.override.conf'
        path "/etc/systemd/system/auditd.service.d/override.conf"
        mode "0644"
        owner "root"
        group "root"
        notifies :run, 'execute[systemctl daemon-reload]', :delayed
    end # of cookbook_file "/etc/systemd/system/auditd.service.d/override.conf" do
end # of if node['init_package'] == 'systemd'

service 'auditd' do
    supports :status => true, :start => true, :stop => true, :restart => true, :reload => true
    action :nothing
end # of service 'auditd' do

cookbook_file "audit rules Datei: cis.rules" do
    source "auditd/cis.rules"
    path "/etc/audit/rules.d/cis.rules"
    mode "0640"
    owner "root"
    group "root"

    # Bei Chef 12.11, mit systemd_unit:
    # notifies :reload_or_try_restart, 'systemd_unit[auditd.service]', :delayed
    # Davor:
    notifies :reload, 'service[auditd]', :delayed
    notifies :restart, 'service[auditd]', :delayed
end # of cookbook_file "audit rules Datei: cis.rules" do

# EOF
