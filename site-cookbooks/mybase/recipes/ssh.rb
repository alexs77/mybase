#
# Cookbook Name:: mybase
# Recipe:: ssh
#
# Copyrigt 2017, Alexander Skwar
#
# All rights reserved - Do Not Redistribute
#

# SSH Konfiguration - Server und Client

# "openssh" Cookbook "ausführen" - erzeugt /etc/ssh/sshd_config
include_recipe "openssh"

#############################################
# Install default .ssh directory
remote_directory "/root/.ssh" do
    files_mode 0o600
    files_owner 'root'
    files_group node['root_group']
    mode 0o700
    owner 'root'
    group node['root_group']
    source "default-ew-.ssh"
end

# #############################################
# # Deploy key for gitlab repository
# remote_directory "/root/.ssh" do
#     files_mode '0600'
#     files_owner 'root'
#     files_group 'root'
#     mode '0700'
#     owner 'root'
#     group 'root'
#     source "root-ssh"
# end
# file "/root/.ssh/id_rsa.pub" do
#     mode '0644'
# end

# EOF
