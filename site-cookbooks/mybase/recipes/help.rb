#
# Cookbook Name:: mybase
# Recipe:: help
#
# Copyrigt 2017, Alexander Skwar
#
# All rights reserved - Do Not Redistribute
#

# Recipe, welches "hilfe" "ausgibt" (ggf. nur im Log)

message = "
Baseline

Ohne Angabe von auszuführenden Recipe(s) werden *KEINE* Änderungen am
System vorgenommen. Folgende Recipes sind vorhanden:

mybase::audit
 => Auditing einrichten (Linux)
mybase::audit-bash
 => Bash (und zsh) Auditing mit bash_franzi Script einrichten. (Alle)
mybase::deb-sources
 => Paketquellen konfigurieren (/etc/apt/sources) (Debian/Ubuntu)
mybase::desktop
 => Ein Desktop System installieren und konfigurieren (Ubuntu)
mybase::disable-floppy
 => Floppy Drive deaktivieren (Linux)
mybase::dns-resolve
 => DNS Resolver (Alle)
mybase::fbsd-sources
 => Paketquellen konfigurieren (FreeBSD)
mybase::hardening
 => Standard System Hardening (Alle)
mybase::ipa-check
 => IPA Checks (Linux)
mybase::logrotate
 => Logrotate (Alle)
mybase::locales
 => Locales (Linux)
mybase::lxc
 => LXC Container spezfische Sachen (Linux)
mybase::misc
 => Verschiedenes (Alle)
mybase::monitoring
 => Icinga Monitoring (Alle)
mybase::no-chef-client
 => Chef Client nicht automatisch ausführen (Linux)
mybase::ntp
 => NTP Client einstellen (Alle)
mybase::pakete
 => Standard Pakete installieren (Linux)
mybase::partitioning
 => Standard Partitionen/LVM LVs erstellen (Alle)
mybase::permissions
 => Standard Berechtigungen bei Dateien & Verzeichnissen setzen (s.a. mybase::hardening) (Alle)
mybase::rh-sources
 => Paketquellen konfigurieren (yum) (Redhat)
mybase::skel
 => /etc/skel Verzeichnis (Alle)
mybase::ssh
 => SSH Daemon (Alle)
mybase::sudo
 => sudo Konfiguration (Alle)
mybase::test
 => Testing (no op) (Alle)
mybase::timezone
 => Timezone einstellen (Alle)
mybase::tmpfs
 => /tmp als tmpfs mounten (Linux)
mybase::useradd-no-groups
 => useradd umkonfigurieren (Alle)
mybase::users
 => Standardnutzer anlegen (Alle)
mybase::vim
 => Vim konfigurieren (Alle)
mybase::zsh
 => Recipe, welches die einzig wahre™ Login Shell _RICHTIG_™^2 konfiguriert (Alle)

Desweiteren:
mybase::all
 => Führt *alle* o.g. Recipes aus, die für die Node (OS und Distrbution) sinnvoll sind (Alle)

Genauere Doku, siehe README.md

Das/Die auszuführende(n) Recipe(s) stelle man entweder in /etc/chef/node.json ein
oder beim chef-client Aufruf mit -o/--override-runlist. Beispiel.

/usr/bin/chef-client -o mybase::dns-resolve,mybase::login-shell -j /etc/chef/node.json -c /opt/kitchen/scripts/systemd/chef-zero.rb
"

log "Hilfe Message ausgeben" do
    message message
    level :info
end # of log "Hilfe Message ausgeben" do

# EOF
