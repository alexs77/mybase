#
# Cookbook Name:: mybase
# Recipe:: tmpfs
#
# Copyrigt 2017, Alexander Skwar
#
# All rights reserved - Do Not Redistribute
#

# Recipe, welches /tmp als tmpfs mounted

# Supported OS: Linux

# Auf systemd Systemen (→ Ubuntu, Debian, Redhat, CentOS, …),
# den "tmp.mount" Service nutzen.
# Ansonsten in /etc/fstab eintragen.

case node[:init_package]
    when 'systemd'
        # Lege systemd Unit für tmp Mount an
        # Falls es "unit_src_path" gibt, wird der Inhalt der Datei ausgelesen
        # und die Unit daraus erzeugt.
        # Falls nicht, wird eine (hoffentlich existierende…) Unit nur aktiviert

        # Paket "manpages" installieren, sonst schlägt das systemd-analyze fehl,
        # weil: Documentation=man:hier(7)
        package "manpages"

        unit_src_path = "/usr/share/systemd/tmp.mount"
        systemd_unit "tmpfs tmp systemd_unit" do
            name "tmp.mount"
            content IO.read(unit_src_path) if ::File.exists?(unit_src_path)
            action [:create, :enable, :start] if ::File.exists?(unit_src_path)
            action [:enable, :start] unless ::File.exists?(unit_src_path)
        end # of systemd_unit "tmpfs tmp systemd_unit" do

    else
        # Kein systemd - lege Mountpoint in /etc/fstab an
        mount "tmp tmpfs mount" do
            device "none"
            mount_point "/tmp"
            fstype "tmpfs"
            options %w[rw nosuid nodev relatime noexec]
            dump 0
            pass 0
            enabled true
            supports [:remount]
            action [:enable, :remount]
        end # of mount "tmp tmpfs mount" do
end # of case node[:init_package]

# EOF
