#
# Cookbook Name:: mybase
# Recipe:: hardening
#
# Copyrigt 2017, Alexander Skwar
#
# All rights reserved - Do Not Redistribute
#

# Hardening des Systems

# Zuerst OS unabhängige Sachen.
include_recipe "#{cookbook_name}::hardening-all"

# Und dann vom OS abhängiges.
include_recipe "#{cookbook_name}::hardening-#{node['os']}"

# EOF
