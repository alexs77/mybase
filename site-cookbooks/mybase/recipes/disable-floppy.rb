#
# Cookbook Name:: mybase
# Recipe:: disable-floppy
#
# Copyrigt 2017, Alexander Skwar
#
# All rights reserved - Do Not Redistribute
#

# Recipe, um das Floppy Drive zu deaktivieren

cookbook_file "Floppy Deaktivieren: /etc/modprobe.d/#{cookbook_name}-blacklist-floppy.conf" do
    source "disable-floppy/modprobe.d/modprobe.d/#{cookbook_name}-blacklist-floppy.conf"
    path "/etc/modprobe.d/modprobe.d/#{cookbook_name}-blacklist-floppy.conf"
    mode "0644"
    owner 0
    group 0
    action :create
    notifies :run, 'execute[update-initramfs]'
    notifies :run, 'execute[remove floppy module]'
end # of cookbook_file "Floppy Deaktivieren: /etc/modprobe.d/#{cookbook_name}-blacklist-floppy.conf" do

execute 'update-initramfs' do
    command '/usr/sbin/update-initramfs -u'
    action :nothing
end

execute 'remove floppy module' do
    command '/sbin/modprobe -r floppy'
    action :nothing
end

# EOF
