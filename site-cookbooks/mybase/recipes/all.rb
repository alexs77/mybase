#
# Cookbook Name:: mybase
# Recipe:: all
#
# Copyright 2015-2017, Alexander Skwar
#
# All rights reserved - Do Not Redistribute
#

# Recipe, welches ein System als standard System konfiguriert

# Es werden alle (wichtigen…) "Sub-Recipes" vom Cookbook "ew" aufgerufen.

# → Neuer Name: meta-default
log "#{cookbook_name}::#{recipe_name} ist deprecated - bitte #{cookbook_name}::meta-default verwenden!"

include_recipe "#{cookbook_name}::meta-default"

# EOF
