#
# Cookbook Name:: mybase
# Recipe:: hardening-all
#
# Copyrigt 2017, Alexander Skwar
#
# All rights reserved - Do Not Redistribute
#

# Hardening des Systems - Alle

#############################################
# Shell Timeout setzen
tmout_guard_re=/^TMOUT=#{node[cookbook_name][:hardening][:shell_timeout]}/
ruby_block "Shell Timeout TMOUT setzen auf " + node[cookbook_name][:hardening][:shell_timeout].to_s + " Sekunden" do
    block do
        fe = Chef::Util::FileEdit.new(node[cookbook_name][:hardening][:bashrc])
        fe.insert_line_if_no_match(tmout_guard_re,
                                   "# Shell Timeout = #{node[cookbook_name][:hardening][:shell_timeout]}s\nTMOUT=#{node["mybase"][:hardening][:shell_timeout]}; readonly TMOUT; export TMOUT\n")
        fe.write_file
    end

    not_if {::File.foreach(node[cookbook_name][:hardening][:bashrc]).grep(tmout_guard_re).any?}
    only_if { ::File.exists?(node[cookbook_name][:hardening][:bashrc]) }
end # of ruby_block "Shell Timeout TMOUT setzen auf " + node[cookbook_name][:hardening][:shell_timeout].to_s + " Sekunden" do

#############################################
# UMASK restriktiv setzen
umask_guard_re=/^umask #{node[cookbook_name][:hardening][:umask]}/
ruby_block "umask restriktiv auf " + node[cookbook_name][:hardening][:umask] + " setzen in " + node["mybase"][:hardening][:bashrc] do
    block do
        fe = Chef::Util::FileEdit.new(node[cookbook_name][:hardening][:bashrc])
        fe.insert_line_if_no_match(umask_guard_re,
                                   "# umask restriktiv setzen auf #{node[cookbook_name][:hardening][:umask]}\numask #{node["mybase"][:hardening][:umask]}\n")
        fe.write_file
    end

    not_if {::File.foreach(node[cookbook_name][:hardening][:bashrc]).grep(umask_guard_re).any?}
    only_if { ::File.exists?(node[cookbook_name][:hardening][:bashrc]) }
end # of ruby_block "umask restriktiv auf " + node[cookbook_name][:hardening][:umask] + " setzen in " + node["mybase"][:hardening][:bashrc] do

umask_defs_guard_re=/^UMASK #{node[cookbook_name][:hardening][:umask]}/
ruby_block "umask restriktiv auf " + node[cookbook_name][:hardening][:umask] + " setzen in /etc/login.defs" do
    block do
        sed = Chef::Util::FileEdit.new("/etc/login.defs")
        sed.search_file_replace(/^(UMASK.*)/,
            '# umask restriktiv setzen auf ' + node[cookbook_name][:hardening][:umask] + "\n# Alt: \\1\nUMASK " + node["mybase"][:hardening][:umask] + "\n"
            )
        sed.write_file
    end

    not_if {::File.foreach("/etc/login.defs").grep(umask_defs_guard_re).any?}
    only_if { ::File.exists?('/etc/login.defs') }
end # of ruby_block "umask restriktiv auf " + umaskdefs + " setzen in /etc/login.defs" do

#############################################
# Login Banner: /etc/issue und /etc/issue
%w[issue issue.net]. each do |bannerfile|
    cookbook_file "Login Banner Datei: /etc/" + bannerfile do
        source 'hardening/' + bannerfile
        path "/etc/" + bannerfile
        mode "0644"
        owner 0
        group 0
        action :create
    end # of cookbook_file "Login Banner Datei: /etc/" + bannerfile do
end # of %w[issue issue.net]. each do |bannerfile|

#############################################
# System File Permissions
node[cookbook_name][:hardening][:file_permissions].each do |filename, filedef|
    file "System File Permissions anpassen: " + filename + " -> Owner: " + filedef[:user].to_s + ":" + filedef[:group].to_s + " Rechte: " + filedef[:perms].to_s do
        path filename
        owner filedef[:owner]
        group filedef[:group]
        mode filedef[:perms]
        only_if { ::File.exists?(filename) }
    end # of file "System File Permissions anpassen: " + filename + " -> Owner: " + filedef[:user].to_s + ":" + filedef[:group].to_s + " Rechte: " + filedef[:perms].to_s do
end # of node[cookbook_name][:hardening][:file_permissions].each do |filename, filedef|

#############################################
# Compass SNB NG Audit
# Setuid Dateien anpassen
# https://wiki.ubuntu.com/Security/Investigation/Setuid
node[cookbook_name][:hardening][:no_setuid_files].each do |setuidfile|
    # https://discourse.chef.io/t/cannot-set-permissions-with-file-resource-to-quoted-string/7259
    # file resource klappt nicht, da "u-s" nicht geht.
    # file "Setuid Berechtigung anpassen: " + setuidfile do
    #     path setuidfile
    #     mode "u-s"
    # end # of file "Setuid Berechtigung anpassen: " + setuidfile do

    execute "Setuid Berechtigung anpassen: " + setuidfile do
        command 'chmod u-s "' + setuidfile + '"'
        only_if { ::File.exists?(setuidfile) && ::File.stat(setuidfile).setuid? }
    end # of execute "Setuid Berechtigung anpassen: " + setuidfile do
end # of %w[/usr/lib/pt_chown /usr/lib/eject/dmcrypt-get-device /usr/bin/chfn].each do |setuidfile|

# EOF
