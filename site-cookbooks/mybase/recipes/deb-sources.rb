#
# Cookbook Name:: mybase
# Recipe:: apt-sources
#
# Copyrigt 2017, Alexander Skwar
#
# All rights reserved - Do Not Redistribute
#

# Recipe, welches die apt/sources.list bzw. sources.list.d Datei(en) einstellt.

# apt/sources.list Datei entfernen
cookbook_file "sources.list Datei ersetzen" do
    source "pakete/sources.list"
    path "/etc/apt/sources.list"
    owner "root"
    group node[:root_group]
    mode 0o404
end # of cookbook_file "sources.list Datei ersetzen" do

# Bei Ubuntu
if node[:platform] == "ubuntu"
    apthost = "http://" + ( node[cookbook_name][:apthost].nil? ? "mirror.unixsrv.everyware.zone" : node["mybase"][:apthost] )

    # Ubuntu Repositories
    [ node['lsb']['codename'], node['lsb']['codename'] + "-security", node['lsb']['codename'] + "-updates" ].each do |dist|
        apt_repository "Ubuntu Repos: " + dist do
            name "Ubuntu-Repositories-" + dist
            uri apthost + "/" + node[cookbook_name][:aptdate] + "/ubuntu"
            distribution dist
            components ["main", "restricted", "multiverse", "universe"]
            deb_src true
        end # of apt_repository "Ubuntu Repos: " + distribution do
    end # of [ node['lsb']['codename'], node['lsb']['codename'] + "-security" ].each do |dist|

    # "ew" Repository einbinden
    apt_repository "ew" do
        uri apthost + "/" + node[cookbook_name][:aptdate] + "/ew"
        distribution node['lsb']['codename']
        components ["main"]
        arch "amd64"
        key apthost + "/" + node[cookbook_name][:aptdate] + "/keys/ew-repo_7CF7F04D.key"
    end # of apt_repository "ew" do

    # "fish-shell" Repository einbinden
    apt_repository "fish-shell" do
        uri apthost + "/" + node[cookbook_name][:aptdate] + "/fish-shell"
        distribution node['lsb']['codename']
        components ["main"]
        arch "amd64"
        key apthost + "/" + node[cookbook_name][:aptdate] + "/keys/fish-6DC33CA5-0x59FDA1CE1B84B3FAD89366C027557F056DC33CA5.key"
    end # of apt_repository "ew" do
end # of if node["platform"] == "ubuntu"

# Vorlage für Proxy Konfiguration
template 'apt proxy Konfigurationsdatei: /etc/apt/apt.conf.d/14proxy' do
    source 'deb-sources/apt.conf.d.14proxy.erb'
    path '/etc/apt/apt.conf.d/14proxy'
    owner 'root'
    group node[:root_group]
    mode 0o644
    action :create_if_missing

    variables({
        :sources => node[cookbook_name][:sources]
    })
end # of template 'apt proxy Konfigurationsdatei: /etc/apt/apt.conf.d/14proxy' do

# EOF
