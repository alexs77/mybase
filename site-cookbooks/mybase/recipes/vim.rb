#
# Cookbook Name:: mybase
# Recipe:: vim
#
# Copyrigt 2017, Alexander Skwar
#
# All rights reserved - Do Not Redistribute
#

# Recipe, welches vim richtig™ konfiguriert.

#############################################
# Setze vim.nox als standard Editor
execute "vim.nox als standard Editor setzen" do
    command '/usr/sbin/update-alternatives --set editor /usr/bin/vim.nox'
    user "root"
    action :nothing
end # of execute "vim.nox als standard Editor setzen" do

# VIM Installation
package "vim-nox installieren" do
    package_name 'vim-nox'
    action :install
    notifies :run, 'execute[vim.nox als standard Editor setzen]', :delayed
end # of package "vim-nox installieren" do

## vim-tiny wird nicht benötigt
#package "vim-tiny deinstallieren" do
#    package_name 'vim-tiny'
#    action :purge
#    only_if "dpkg-query --show vim-tiny > /dev/null 2>&1"
#end # of package "vim-nox installieren" do

# VIM Anpassung
cookbook_file "/etc/vim/vimrc.local" do
    source "vimrc.local"
    mode 0o644
    owner "root"
    group node['root_group']
end

# EOF
