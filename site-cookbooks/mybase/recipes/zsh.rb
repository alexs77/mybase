#
# Cookbook Name:: mybase
# Recipe:: zsh
#
# Copyrigt 2017, Alexander Skwar
#
# All rights reserved - Do Not Redistribute
#

# Recipe, welches die einzig wahre™ Login Shell _RICHTIG_™^2 konfiguriert

#############################################
# zsh Installation
package "ZSH installieren: " + node[cookbook_name][:zsh][:pakete].to_s do
    package_name node[cookbook_name][:zsh][:pakete]
    action :install
end # of package "ZSH installieren: " + node[cookbook_name][:zsh][:pakete].to_s do

# zsh Konfiguration
cookbook_file "/etc/zsh/zshrc" do
    path '/etc/zsh/zshrc'
    source "zsh/zshrc"
    mode 0o644
    owner "root"
    group node[:root_group]
end # of cookbook_file "/etc/zsh/zshrc" do

# EOF
