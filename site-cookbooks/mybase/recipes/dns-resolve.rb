#
# Cookbook Name:: mybase
# Recipe:: dns-resolve
#
# Copyrigt 2017, Alexander Skwar
#
# All rights reserved - Do Not Redistribute
#

# Recipe, welches ein DNS Resolving konfiguriert

# "resolver" Cookbook "ausführen" - erzeugt /etc/resolv.conf
include_recipe 'resolver'

# EOF
