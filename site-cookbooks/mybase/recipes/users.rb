#
# Cookbook Name:: mybase
# Recipe:: users
#
# Copyright 2015-2017, Alexander Skwar
#
# All rights reserved - Do Not Redistribute
#

# Recipe, welches Benutzer anlegt.

# User sind im Hash node[cookbook_name][:users] zu definieren. Syntax, siehe
# attributes/users.rb.
# Falls node[cookbook_name][:create_admin_users] auf true gesetzt ist, werden
# auch Standard Admin User (gemäß node[cookbook_name][:admin_users]) angelegt.

users = {}
users.merge!(node[cookbook_name][:users])
users.merge!(node[cookbook_name][:admin_users]) if node["mybase"][:create_admin_users]

# Lege "remote-login" Grupe an - Nur Mitglieder dieser Gruppe dürfen
# per SSH einen Remote Login machen.
group "Gruppe remote-login anlegen" do
    group_name "remote-login"
    append true
    system true
end # of group "Gruppe remote-login anlegen" do

# Lege alle User an
users.each do |username, userdef|
    next if userdef.nil?

    # Zuerst die Gruppe anlegen
    group "#{cookbook_name} Gruppe anlegen: " + username do
        group_name username
        gid userdef[:gid].to_i unless userdef[:gid].nil? or userdef[:gid].to_s.empty?
    end # of group "#{cookbook_name} Gruppe anlegen: " + username do

    # Falls es den Account schon gibt, überspringen
    #next unless `getent passwd ${username}` == ""

    # Kennwort setzen - user resource -> password geht nicht, weil: "You must have ruby-shadow installed for password support!"
    # usermod manuell aufrufen (falls ein Benutzer angelegt worden ist)
    execute "#{cookbook_name} Benutzer #{username} Kennwort setzen" do
        command "usermod -p '#{userdef[:password_hash]}' '#{username}'"
        sensitive true
        # Wird bei Bedarf von user angestossen
        action :nothing
        # Nur auf Linux ausführen
        only_if { node['os'] == "linux" }
        # Nicht ausführen, falls kein Kennwort definiert
        not_if { userdef[:password_hash].nil? or userdef[:password_hash].empty? }
    end # of execute "#{cookbook_name} Benutzer #{username} Kennwort setzen" do

    user "#{cookbook_name} Benutzer anlegen: #{username}" do
        username username
        uid userdef[:uid].to_i unless userdef[:uid].nil? or userdef[:uid].to_s.empty?
        gid userdef[:gid].to_i unless userdef[:gid].nil? or userdef[:gid].to_s.empty?
        comment userdef[:comment] unless userdef[:comment].nil? or userdef[:comment].to_s.empty?
        home userdef[:home] unless userdef[:home].nil? or userdef[:home].to_s.empty?
        shell userdef[:login_shell] unless userdef[:login_shell].nil? or userdef[:login_shell].to_s.empty?
        manage_home true
        # Kennwort setzen - user resource -> password geht nicht, weil: "You must have ruby-shadow installed for password support!"
        # Falls was gemacht wurde, Kennwort setzen
        #password '$1$JJsvHslasdfjVEroftprNn4JHtDi'
        notifies :run, "execute[#{cookbook_name} Benutzer #{username} Kennwort setzen]", :immediately
    end # of user "#{cookbook_name} Benutzer anlegen: #{username}" do

    directory "#{cookbook_name} Benutzer .ssh Verzeichnis anlegen: #{userdef[:home]}/.ssh" do
        path userdef[:home].to_s + '/.ssh'
        owner userdef[:uid].to_i unless userdef[:uid].nil? or userdef[:uid].to_s.empty?
        group userdef[:gid].to_i unless userdef[:gid].nil? or userdef[:gid].to_s.empty?
        recursive true
        mode 0o700
        not_if {userdef[:home].nil? or userdef[:home].empty?}
    end # of directory "#{cookbook_name} Benutzer .ssh Verzeichnis anlegen: #{userdef[:home]}/.ssh" do

    # Falls userdef[:authorized_keys] definiert ist, Inhalt
    # nach ~/.ssh/authorized_keys schreiben.
    # Falls nicht, dann das Cookbook File "users/#{username}.authorized_keys"
    # verwenden und Fehler ignorieren - Okay, wenn Cookbokks File Quell-Datei
    # nicht vorhanden ist.
    if userdef[:authorized_keys].nil? or userdef[:authorized_keys].empty?
        cookbook_file "#{cookbook_name} Benutzer authorized_keys kopieren: #{userdef[:home]}/.ssh/authorized_keys" do
            source 'users/' + username + '.authorized_keys'
            path userdef[:home].to_s + '/.ssh/authorized_keys'
            owner userdef[:uid].to_i unless userdef[:uid].nil? or userdef[:uid].to_s.empty?
            group userdef[:gid].to_i unless userdef[:gid].nil? or userdef[:gid].to_s.empty?
            mode 0o600
            ignore_failure true
            not_if {userdef[:home].nil? or userdef[:home].empty?}
        end # of cookbook_file "#{cookbook_name} Benutzer authorized_keys kopieren: #{userdef[:home]}/.ssh/authorized_keys" do
    else
        file "#{cookbook_name} Benutzer authorized_keys anlegen: #{userdef[:home]}/.ssh/authorized_keys" do
            content userdef[:authorized_keys]              if userdef[:authorized_keys].is_a? String
            content userdef[:authorized_keys].join("\n")   if userdef[:authorized_keys].is_a? Array

            path userdef[:home].to_s + '/.ssh/authorized_keys'
            owner userdef[:uid].to_i unless userdef[:uid].nil? or userdef[:uid].to_s.empty?
            group userdef[:gid].to_i unless userdef[:gid].nil? or userdef[:gid].to_s.empty?
            mode 0o600
            not_if {userdef[:home].nil? or userdef[:home].empty?}
        end # of file "#{cookbook_name} Benutzer authorized_keys anlegen: #{userdef[:home]}/.ssh/authorized_keys"
    end # of if userdef[:authorized_keys].nil? or userdef[:authorized_keys].empty? do

    unless userdef[:groups].nil? or userdef[:groups].empty?
        userdef[:groups].each do |group_name|
            group '#{cookbook_name} Benutzer ' + username + ' zur Gruppe ' + group_name + ' hinzufügen' do
                group_name group_name
                members username
                append true
                # Nur was machen, falls es den Account (inzwischen) gibt
                only_if "getent passwd #{username}"
            end # of group '#{cookbook_name} Benutzer ' + username + ' zur Gruppe ' + group_name + ' hinzufügen' do
        end # of userdef[:groups].each do |group_name|
    end # of unless userdef[:groups].empty?
end # of node[cookbook_name][:users].each do |username, userdef|

# Entferne User und Gruppe
node[cookbook_name][:admin_users_remove].each do |username|
    user "Benutzer entfernen: #{username}" do
        username username
        action :remove
        force true
        manage_home true
    end # of user "Benutzer entfernen: #{username}" do

    group "Gruppe entfernen: #{username}" do
        group_name username
        action :remove
    end # of group "Gruppe entfernen: #{username}" do
end unless node[cookbook_name][:admin_users_remove].nil? # of node[cookbook_name][:admin_users_remove].each do |username|

# EOF
