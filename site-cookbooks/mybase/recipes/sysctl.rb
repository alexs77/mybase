#
# Cookbook Name:: mybase
# Recipe:: sysctl
#
# Copyrigt 2017, Alexander Skwar
#
# All rights reserved - Do Not Redistribute
#

# Recipe, welches Kernel Parameter mit sysctl einstellt.

# => https://supermarket.chef.io/cookbooks/sysctl

# ohai Plugin laden
#include_recipe 'sysctl::ohai_plugin'

#node.default['sysctl'] = node[cookbook_name][:sysctl]

# Parameter setzen
#include_recipe 'sysctl::apply'

include_recipe 'sysctl::default'

node[cookbook_name][:sysctl][:params].each do |name, value|
    sysctl_param 'Setze sysctl Parameter ' + name + ': ' + value.to_s do
        key name
        value value
        # Nur dann, wenn es den Kernelparameter gibt.
        only_if { ::File.exists?( "/proc/sys/" + name.gsub('.', '/') ) }
        # Aber nicht, wenn er schon den richtigen Wert hat.
        not_if "sysctl --values #{name} | grep '^#{value}$'"
    end # of sysctl_param 'Setze sysctl Parameter ' + name + ': ' + value.to_s do
end # of node[cookbook_name][:sysctl][:params].each do |name, value|

# EOF

