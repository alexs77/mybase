#
# Cookbook Name:: mybase
# Recipe:: locales
#
# Copyrigt 2017, Alexander Skwar
#
# All rights reserved - Do Not Redistribute
#

# Recipe, welches die "locales" auf dem System installiert.

#############################################
# Locale

execute 'run-locale-gen' do
    command 'locale-gen'
    action :nothing
end # of execute 'run-locale-gen' do

directory 'supported locales Verzeichnis' do
    owner "root"
    group node[:root_group]
    mode 0o775
    path "/var/lib/locales/supported.d"
end # of directory 'supported locales Verzeichnis' do

# cookbook_file "ew standard locales" do
#     source "locales"
#     path "/var/lib/locales/supported.d/ew-standard"
#     mode 0o664
#     owner "root"
#     group node[:root_group]
#     notifies :run, 'execute[run-locale-gen]', :immediately
# end # of cookbook_file "ew standard locales" do

template "#{cookbook_name} standard locales" do
    source "locales/supported.erb"
    path "/var/lib/locales/supported.d/#{cookbook_name}-standard"
    mode 0o644
    owner "root"
    group node[:root_group]
    notifies :run, 'execute[run-locale-gen]', :immediately
    variables({
        :supported_locales => node[cookbook_name][:locales][:supported]
    })
end # of template "ew standard locales" do

execute "update-locale" do
    command "/usr/sbin/update-locale #{node[cookbook_name][:locales][:default]}"
    user "root"
    not_if {"#{node[cookbook_name][:locales][:default]}".empty?}
end # of execute "update-locale" do

# EOF

