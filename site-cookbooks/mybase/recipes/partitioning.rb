#
# Cookbook Name:: mybase
# Recipe:: partitioning
#
# Copyrigt 2017, Alexander Skwar
#
# All rights reserved - Do Not Redistribute
#

# Recipe, welches die Standard "Partitionen" auf dem System erzeugt.

# vom OS abhängiges.
include_recipe "#{cookbook_name}::partitioning-#{node["os"]}"

# EOF
