#
# Cookbook Name:: mybase
# Recipe:: no-chef-client
#
# Copyrigt 2017, Alexander Skwar
#
# All rights reserved - Do Not Redistribute
#

# Recipe, welches den chef-client Service deaktiviert.

# Disable chef-client (we run chef-solo or chef-zero in this kitchen)
service "Disable chef-client Service" do
    service_name "chef-client"
    pattern 'ruby /usr/bin/chef-client'
    action [ :stop, :disable ]
    supports :status => true, :restart => true, :reload => true
end # of service "Disable chef-client Service" do

# EOF
