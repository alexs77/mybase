#
# Cookbook Name:: desktop
# Recipe:: misc
#
# Copyrigt 2017, Alexander Skwar
#
# All rights reserved - Do Not Redistribute
#

# Recipe, um ein Desktop System zu installieren/konfigurieren

# Auch die Desktop Pakete installieren
node.default[cookbook_name][:packages][:sets] += [:desktop, :desktop_common]

# Den Ubuntu Mirror über die interne IP ansprechen
node.default[cookbook_name][:apthost] = 'mirror.unixsrv.everyware.zone'

# ew Standard anwenden
include_recipe "#{cookbook_name}::all"

# EOF
