#
# Cookbook:: mybase
# Recipe:: timezone
#
# Copyright:: 2017, Alexander Skwar, All Rights Reserved.

# Timezone Konfiguration

# Bei Debian (Ubuntu) Datei /etc/timezone aktualisieren
file "/etc/timezone" do
    content node["mybase"]["timezone"] + "\n"
    mode "0644"
    owner "root"
    group node["root_group"]

    only_if { node['platform_family'] == 'debian' }
end # of file "/etc/timezone" do

# Auf Linux Symlink /etc/localtime aktualisieren
link "/etc/localtime Linux" do
    target_file "/etc/localtime"
    to ::File.join("/usr/share/zoneinfo", node["mybase"]["timezone"])

    only_if { node['os'] == 'linux' }
end # of link "/etc/localtime Linux" do

# Auf FreeBSD /etc/localtime aus /usr/share/zoneinfo kopieren
remote_file "/etc/localtime FreeBSD" do
    path "/etc/localtime"
    source "file://" + ::File.join("/usr/share/zoneinfo", node["mybase"]["timezone"])
    mode "0644"
    owner "root"
    group node["root_group"]

    only_if { node['os'] == 'freebsd' }
end # of remote_file "/etc/localtime FreeBSD" do

# EOF
