#
# Cookbook Name:: mybase
# Recipe:: sudoers
#
# Copyrigt 2017, Alexander Skwar
#
# All rights reserved - Do Not Redistribute
#

# Sudo Konfiguration.

# Lege sudoers.d/ Dateien an
#
#############################################

cookbook_file "sudoers.d/nopw Datei anlegen" do
    source "sudoers-nopw"
    path "/etc/sudoers.d/everyware-nopw"
    mode 0o644
    owner "root"
    group node[:root_group]
end # of cookbook_file "sudoers.d/nopw Datei anlegen" do

# EOF
