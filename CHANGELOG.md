Änderungen
==========

- 2018-04-18: v1.3.0: Als standard Resolver "1.1.1.1" verwenden
- 2018-04-18: v1.3.0: Nicht "links2" Paket installieren
- 2018-04-18: v1.3.0: minimale Chef Version auf 12.14 reduziert, wegen Ubuntu Bionic
- 2018-04-18: v1.3.0: Bei "users" auch Benutzer entfernen
- 2017-10-05: v1.1.0: timezone Recipe angelegt
- 2017-10-04: v1.0.3: Standard Benutzer "user" anlegen; "local" nicht mehr
- 2017-08-22: v1.0.1: Keine Abhängigkeit vom `apt` Cookbook
- 2017-08-21: v3.1.0: Verzeichnis `/var/lib/locales` anlegen lassen (für locales)
- 2017-07-13: `monitoring` Recipe wird nach `icinga-client`
  Cookbook ausgelagert
